﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DC4B.FilmsBrowser.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Film> Films { get; set; }
    }
}
