﻿using System.Collections.Generic;

namespace DC4B.FilmsBrowser.Entities
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}