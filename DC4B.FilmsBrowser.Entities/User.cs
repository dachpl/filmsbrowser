﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DC4B.FilmsBrowser.Entities
{
    public class User : BaseEntity
    {
        /// <summary>
        /// Email == Login
        /// </summary>
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
