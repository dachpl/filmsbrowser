﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.Entities
{
    public class Film : BaseEntity
    {
        public string Name { get; set; }
        public DateTime PremiereDate { get; set; }
        public int CategoryId { get; set; }
        
        public virtual Category Category { get; set; }
        public virtual ICollection<Actor> Actors { get; set; }
    }
}
