﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DC4B.FilmsBrowser.Entities
{
    public class Actor : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public virtual ICollection<Film> Films { get; set; }
    }
}
