﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using DC4B.FilmsBrowser.Common;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using DC4B.FilmsBrowser.Services.Concrete;
using LightInject;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        internal static ServiceContainer DIContainer = new ServiceContainer();
        protected void Application_Start()
        {
            ConfigureDependencyInjection();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            

            Mappings.CreateMaps();
            ConfigureJsonFormatter();
        }

        private static void ConfigureDependencyInjection()
        {
            DIContainer.RegisterApiControllers();
            DIContainer.EnableWebApi(GlobalConfiguration.Configuration);

            //BINDINGS
            DIContainer.Register<DbContext, DataContext>();
            DIContainer.Register(typeof(ISqlRepository<>), typeof(SqlRepository<>)); //can be commented because there is no direct call to ISqlRepository
            DIContainer.Register<ISqlUnitOfWork, SqlUnitOfWork>();
            DIContainer.Register<ICategoryService, CategoryService>();
            DIContainer.Register<IMembershipService, MembershipService>();
            DIContainer.Register<IInitialViewService, InitialViewService>();
            DIContainer.Register<IFilmService, FilmService>();
            DIContainer.Register<IActorService, ActorService>();
            DIContainer.Register<IDashboardService, DashboardService>();
            DIContainer.Register<IUserService, UserService>();
        }


        private static void ConfigureJsonFormatter()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling =  Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            //? do we need this?
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            
        }
    }
}
