﻿using Microsoft.Owin.Security.Jwt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DC4B.FilmsBrowser.API.Security
{
    public class JWTOptions : JwtBearerAuthenticationOptions
    {
        public JWTOptions()
        {
            var issuer = AppSettings.Owin_Issuer;
            var audience = AppSettings.Owin_Audience;
            var key = Convert.FromBase64String(AppSettings.Owin_Base64Key);

            AllowedAudiences = new[] { audience };
            IssuerSecurityTokenProviders = new[]
            {
                new SymmetricKeyIssuerSecurityTokenProvider(issuer, key)
            };
        }
    }
}