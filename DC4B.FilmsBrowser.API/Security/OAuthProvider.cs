﻿using DC4B.FilmsBrowser.Common.ApiVM.Membership;
using DC4B.FilmsBrowser.Services.Abstract;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.API.Security
{
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly IMembershipService _membershipService;
        public OAuthProvider(IMembershipService membershipService)
        {
            _membershipService = membershipService;
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ClaimsIdentity identity = new ClaimsIdentity(AppSettings.Owin_ClaimAuthType);
            var username = context.OwinContext.Get<string>(AppSettings.Owin_ClaimAuthUsername);
            identity.AddClaim(new Claim(AppSettings.Owin_ClaimName, username));

            LoggedInVM userData = _membershipService.GetUserData(username);

            identity.AddClaim(new Claim("UserId", userData.UserId.ToString()));
            userData.Roles.ToList().ForEach(role =>
            {
                identity.AddClaim(new Claim(AppSettings.Owin_ClaimRole, role.Name));
            });

            var ticket = new AuthenticationTicket(identity, null);
            context.Validated(ticket);
            return Task.FromResult(0);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var username = context.Parameters["username"];
            var password = context.Parameters["password"];
            
            bool validLogin = _membershipService.VerifyLogin(username);
            if (!validLogin)
            {
                context.SetError("Invalid username");
                context.Response.Headers.Add(AppSettings.Owin_ChallengeFlag, new[] { ("") });
                return Task.FromResult(0);
            }
            
            bool validPassword = _membershipService.VerifyPassword(username, password);
            if (!validPassword)
            {
                context.SetError("Invalid password");
                context.Response.Headers.Add(AppSettings.Owin_ChallengeFlag, new[] { ("") });
                return Task.FromResult(0);
            }

            context.OwinContext.Set(AppSettings.Owin_ClaimAuthUsername, username);
            context.Validated();
            
            return Task.FromResult(0);
        }


    }
}