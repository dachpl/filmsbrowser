﻿using DC4B.FilmsBrowser.Services.Abstract;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DC4B.FilmsBrowser.API.Security
{
    public class OAuthOptions : OAuthAuthorizationServerOptions
    {
        public OAuthOptions()
        {
            TokenEndpointPath = new Microsoft.Owin.PathString("/api/token");
            AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(int.Parse(AppSettings.Owin_TokenExpirationInMinutes));
            AccessTokenFormat = new JWTFormat(this);
            Provider = new OAuthProvider(WebApiApplication.DIContainer.GetInstance<IMembershipService>());
#if DEBUG
            AllowInsecureHttp = true;
#endif
        }
    }
}