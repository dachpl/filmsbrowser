﻿using System.Configuration;

namespace DC4B.FilmsBrowser.API
{
    [System.Diagnostics.DebuggerNonUserCodeAttribute]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute]
    public static class AppSettings
    {
        public static string Owin_Audience
        {
            get { return ConfigurationManager.AppSettings["Owin_Audience"]; }
        }

        public static string Owin_Base64Key
        {
            get { return ConfigurationManager.AppSettings["Owin_Base64Key"]; }
        }

        public static string Owin_ChallengeFlag
        {
            get { return ConfigurationManager.AppSettings["Owin_ChallengeFlag"]; }
        }

        public static string Owin_ClaimAuthType
        {
            get { return ConfigurationManager.AppSettings["Owin_ClaimAuthType"]; }
        }

        public static string Owin_ClaimAuthUsername
        {
            get { return ConfigurationManager.AppSettings["Owin_ClaimAuthUsername"]; }
        }

        public static string Owin_ClaimName
        {
            get { return ConfigurationManager.AppSettings["Owin_ClaimName"]; }
        }

        public static string Owin_ClaimRole
        {
            get { return ConfigurationManager.AppSettings["Owin_ClaimRole"]; }
        }

        public static string Owin_DigestAlgorithm
        {
            get { return ConfigurationManager.AppSettings["Owin_DigestAlgorithm"]; }
        }

        public static string Owin_Issuer
        {
            get { return ConfigurationManager.AppSettings["Owin_Issuer"]; }
        }

        public static string Owin_SigantureAlgorithm
        {
            get { return ConfigurationManager.AppSettings["Owin_SigantureAlgorithm"]; }
        }

        public static string Owin_TokenExpirationInMinutes
        {
            get { return ConfigurationManager.AppSettings["Owin_TokenExpirationInMinutes"]; }
        }
    }
}

