﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using DC4B.FilmsBrowser.API.Security;

[assembly: OwinStartup(typeof(DC4B.FilmsBrowser.API.App_Start.Startup))]

namespace DC4B.FilmsBrowser.API.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseOAuthAuthorizationServer(new OAuthOptions());
            app.UseJwtBearerAuthentication(new JWTOptions());
            //app.Use<OwinAuthMiddleware>();
        }
    }
}
