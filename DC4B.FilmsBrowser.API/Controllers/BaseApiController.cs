﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DC4B.FilmsBrowser.API.Filters;
using System;
using System.Security.Claims;

namespace DC4B.FilmsBrowser.API.Controllers
{
    public class BaseApiController : ApiController
    {
        public virtual IHttpActionResult Error(HttpStatusCode errorCode, string message)
        {
            return ResponseMessage(Request.CreateErrorResponse(errorCode, message));
        }

        protected int GetUserId()
        {
            var identity = User.Identity as ClaimsIdentity;
            var id = Int32.Parse(identity.Claims.Single(x => x.Type == "UserId").Value);
            return id;
        }
    }
}