﻿using System.Web.Http;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using System.Security.Claims;
using System.Linq;
using System.Web.Security;
using System;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [ModelValidation]
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/users")]
    public class UsersController : BaseCRUDController<User, UserVM>
    {
        private readonly IUserService _userService;
        public UsersController(IUserService service) : base(service)
        {

        }

        [HttpGet, Route("claims")]
        public IHttpActionResult UserIdentity()
        {
            var identity = User.Identity as ClaimsIdentity;

            var claims = from c in identity.Claims
                         select new
                         {
                             subject = c.Subject.Name,
                             type = c.Type,
                             value = c.Value
                         };

            return Ok(claims);
        }


    }
}
