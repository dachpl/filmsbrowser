﻿using System.Web.Http;
using DC4B.FilmsBrowser.Services.Abstract;
using System.Security.Claims;
using System.Linq;
using System;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;
using System.Net.Http;
using System.Net;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [Authorize(Roles = "Member")]
    [RoutePrefix("api/initialView")]
    public class InitialViewController : BaseApiController
    {
        private readonly IInitialViewService _initialViewService;
        public InitialViewController(IInitialViewService initialViewService)
        {
            _initialViewService = initialViewService;
        }

        public IHttpActionResult Get()
        {
            var data = _initialViewService.GetInitialViewData(GetUserId());
            return Ok(data);
        }

        public IHttpActionResult Put([FromBody] UserVM userVM)
        {
           _initialViewService.UpdateUser(userVM);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }
    }
}
