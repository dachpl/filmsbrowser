﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AutoMapper;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Common.ApiVM.Category;
using DC4B.FilmsBrowser.Common.ApiVM.Film;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;

namespace DC4B.FilmsBrowser.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ModelValidation]
    [Authorize(Roles = "Member")]
    [RoutePrefix("api/categories")]
    public class CategoriesController : BaseCRUDController<Category, CategoryVM>
    {
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryService"></param>
        public CategoriesController(ICategoryService categoryService) : base(categoryService)
        {
            _categoryService = categoryService;
        }

        #region Custom Actions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [Route("{categoryId:int}/films")]
        public IHttpActionResult GetFilms(int categoryId)
        {
            var category = _categoryService.GetById(categoryId);
            if (category == null)
                return Error(HttpStatusCode.NotFound, "Category not found");

            var data = _categoryService.GetFilms(categoryId);

            return Ok(new
            {
                Category = new
                {
                    Id = category.Id,
                    Name = category.Name
                },
                Films = Mapper.Map<IList<CategoryFilmVM>>(data)
            });
        }
        #endregion
    }
}
