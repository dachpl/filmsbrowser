﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData;
using AutoMapper;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Common.ApiVM.Film;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [ModelValidation]
    [Authorize(Roles = "Member, Admin")]
    [RoutePrefix("api/films")]
    public class FilmsController : BaseCRUDController<Film, FilmVM>
    {
        private readonly IFilmService _filmService;
        public FilmsController(IFilmService filmService)
            : base(filmService)
        {
            _filmService = filmService;
        }

        [HttpGet, Route("create")]
        public IHttpActionResult Create()
        {
            return Ok(_filmService.GetMetadata());
        }
        
        [HttpGet, Route("{filmId}/update")]
        public IHttpActionResult Update(int filmId)
        {
            var filmData = _filmService.GetById(filmId);
            var actorsArray = filmData.Actors.Select(actor => actor.Id).ToList();

            var filmVMData = new FilmVM
            {
                Id = filmData.Id,
                Name = filmData.Name,
                CategoryId = filmData.CategoryId,
                PremiereDate = filmData.PremiereDate,
                Actors = actorsArray.ToArray()
            };
            var metadata = _filmService.GetMetadata();
            return Ok(new {@Film = filmVMData, @Metadata = metadata});
        }

        [EnableQuery]
        public override IHttpActionResult Get()
        {
            return Ok(_filmService.GetFilmsList());
        }

        [EnableQuery]
        public override IHttpActionResult Get(int id)
        {
            var film = _filmService.GetById(id);
            return Ok(new
            {
                Id = film.Id,
                Actors = film.Actors.Select(a => new
                {
                    Id = a.Id,
                    Name = "Test" //$"{a.FirstName} {a.LastName}"
                }),
                CategoryId = film.CategoryId,
                Category = film.Category.Name,
                Name = film.Name,
                PremiereDate = film.PremiereDate
            });
        }

        public override IHttpActionResult Post([FromBody] FilmVM model)
        {
            ICollection<Actor> actors = null;
            if (model.Actors != null)
                actors = _filmService.GetActorsByIds(model.Actors).ToList();

            var ctg = _filmService.GetCategoryById(model.CategoryId);

            var entity = new Film
            {
                Name = model.Name,
                PremiereDate = model.PremiereDate,
                CategoryId = model.CategoryId,
                Category = ctg,
                Actors = actors
            };
            var id = _filmService.Create(entity);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, id));
        }

        public override IHttpActionResult Put(int id, FilmVM model)
        {
            var actors = _filmService.GetActorsByIds(model.Actors).ToList();
            var ctg = _filmService.GetCategoryById(model.CategoryId);
            var entity = new Film
            {
                Id = model.Id,
                Name = model.Name,
                PremiereDate = model.PremiereDate,
                CategoryId = model.CategoryId,
                Category = ctg,
                Actors = actors
            };
            _filmService.Update(entity);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }
    }
}
