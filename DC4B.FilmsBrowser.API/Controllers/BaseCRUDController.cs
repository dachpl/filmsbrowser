﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData;
using AutoMapper;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;

namespace DC4B.FilmsBrowser.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TE">Type of Entity</typeparam>
    /// <typeparam name="TV">Type of Basic View Model</typeparam>
    public abstract class BaseCRUDController<TE, TV> : BaseApiController 
        where TE : BaseEntity where TV : class
    {
        protected IBaseService<TE> Service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        protected BaseCRUDController(IBaseService<TE> service)
        {
            Service = service;
        }

        
        #region Api Convention: GET POST PUT DELETE

        /// <summary>
        /// Get All (TEMPLATE)
        /// </summary>
        /// <returns>Get list of resources</returns>
        [EnableQuery]
        public virtual IHttpActionResult Get()
        {
            var data = Service.GetAll();
            return Ok(Mapper.Map<IList<TV>>(data));
            //return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<IList<TV>>(data)));
        }

        /// <summary>
        /// Get resource by specified Id (TEMPLATE)
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>Resource</returns>
        public virtual IHttpActionResult Get(int id)
        {
            var data = Service.GetById(id);
            if (data == null)
                return Error(HttpStatusCode.NotFound, "Entity not found");
            return Ok(Mapper.Map<TV>(data));
        }

        /// <summary>
        /// Create resource (TEMPLATE)
        /// </summary>
        /// <param name="model">Resource model from body</param>
        /// <returns>New created resource Id</returns>
        public virtual IHttpActionResult Post([FromBody] TV model)
        {
            var entity = Mapper.Map<TE>(model);
            var id = Service.Create(entity);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, id));
        }

        /// <summary>
        /// Update Resource (TEMPLATE)
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <param name="model">Resource model from body</param>
        /// <returns>204 HttpCode (No Content)</returns>
        public virtual IHttpActionResult Put(int id, [FromBody] TV model)
        {
            var entity = Mapper.Map<TE>(model);
            entity.Id = id;

            Service.Update(entity);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }

        /// <summary>
        /// Delete resource (TEMPLATE)
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>204 HttpCode (No Content)</returns>
        public virtual IHttpActionResult Delete(int id)
        {
            Service.Delete(id);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent));
        }

        #endregion
    }
}