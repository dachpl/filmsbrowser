# README #

Application for films management - created for **demonstration purposes**. Browse source code [**here**](https://bitbucket.org/diwadu/filmsbrowser/src)
##Author##
Dawid Chrzanowski, **diwadu@gmail.com**
## How do I get set up? ##
1. Clone this repo or download source code [**here**](https://bitbucket.org/diwadu/filmsbrowser/downloads) and unpack 
2. Install **VS 2015 + SQL Server 2008 Express or higher**
3. Be sure you have installed **node, npm, grunt, bower, yeoman, [generator-angular](https://github.com/yeoman/generator-angular)**
4. Go in console to **Client/WebClient** folder in main directory
5. Run installers **$ npm install, $ bower install**
6. Run VS Project - REST Server **(DC4B.FilmsBrowser.API)**, go to console and run Client App: **$ grunt serve**
## Technologies/frameworks ##
### Backend ###
ASP.NET MVC WebAPI 2.2, Enity Framework 6, SQL Server, Owin, Json Web Token, AutoMapper, LightInject, JSON.NET
![backend_stack.png](https://bitbucket.org/repo/oj4RM5/images/4166830784-backend_stack.png)
### Frontend ###
AngularJS, Twitter Bootstrap, font-awesome, ion-icons, AdminLTE, JWT, npm, grunt, bower, yeoman
![frontend_stack2.png](https://bitbucket.org/repo/oj4RM5/images/3490533493-frontend_stack2.png)
## Example Screenshot ##
![sh_films.png](https://bitbucket.org/repo/oj4RM5/images/2024725543-sh_films.png)