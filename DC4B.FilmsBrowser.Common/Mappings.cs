﻿using System.Linq;
using AutoMapper;
using DC4B.FilmsBrowser.Common.ApiVM.Actor;
using DC4B.FilmsBrowser.Common.ApiVM.Category;
using DC4B.FilmsBrowser.Common.ApiVM.Film;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;
using DC4B.FilmsBrowser.Entities;
using System;

namespace DC4B.FilmsBrowser.Common
{
    public static class Mappings
    {
        public static void CreateMaps()
        {
            Mapper.CreateMap<CategoryVM, Category>(); //for POST PUT 
            Mapper.CreateMap<Category, CategoryVM>(); //for GET

            Mapper.CreateMap<Film, FilmVM>();//ConvertUsing(film => film.Actors);//.ForMember(vm => vm.Actors, opt => opt.MapFrom(s => s.Actors));
            Mapper.CreateMap<FilmVM, Film>(); //.ForMember(dest => dest.Actors, opts=>opts.MapFrom(src => src.Actors));

            Mapper.CreateMap<Film, CategoryFilmVM>();
            Mapper.CreateMap<Film, FilmListItemVM>();

            Mapper.CreateMap<Actor, ActorVM>().ForMember(vm => vm.Films, opt => opt.Ignore());
            Mapper.CreateMap<ActorVM, Actor>();
            Mapper.CreateMap<Actor, ActorWithoutFilmsVM>().ForMember(vm => vm.BirthDate, opt=>opt.MapFrom(src => src.BirthDate.ToString("dd/MM/yyyy")));
            
            Mapper.CreateMap<User, UserVM>();
        }

        
    }
}
