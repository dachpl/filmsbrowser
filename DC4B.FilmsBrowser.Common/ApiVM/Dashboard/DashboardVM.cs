﻿using DC4B.FilmsBrowser.Common.ApiVM.InitialView;

namespace DC4B.FilmsBrowser.Common.ApiVM.Dashboard
{
    public class DashboardVM
    {
        public UserVM User { get; set; }
        public int ActorsCnt { get; set; }
        public int CategoriesCnt { get; set; }
        public int FilmsCnt { get; set; }

    }
}
