﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Common.ApiVM.Actor;
using DC4B.FilmsBrowser.Common.ApiVM.Category;

namespace DC4B.FilmsBrowser.Common.ApiVM.Film
{
    public class FilmMetadataVM
    {
        public IEnumerable<CategoryVM> Categories { get; set; }
        public IEnumerable<ActorWithoutFilmsVM> Actors { get; set; }
    }
}
