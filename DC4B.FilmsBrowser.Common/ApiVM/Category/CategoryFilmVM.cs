﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.Common.ApiVM.Category
{
    public class CategoryFilmVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime PremiereDate { get; set; }
    }
}
