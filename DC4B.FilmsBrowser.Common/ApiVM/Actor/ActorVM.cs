﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Common.ApiVM.Film;

namespace DC4B.FilmsBrowser.Common.ApiVM.Actor
{
    public class ActorVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        public DateTime BirthDate { get; set; }

        public virtual IList<FilmVM> Films { get; set; }
    }
}
