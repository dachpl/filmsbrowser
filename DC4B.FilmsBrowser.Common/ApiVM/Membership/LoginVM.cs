﻿using System.ComponentModel.DataAnnotations;

namespace DC4B.FilmsBrowser.Common.ApiVM.Membership
{
    public class LoginVM
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email field is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password field is required")]
        public string Password { get; set; }
    }
}
