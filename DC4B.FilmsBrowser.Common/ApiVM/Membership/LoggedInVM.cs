﻿using DC4B.FilmsBrowser.Entities;
using System.Collections.Generic;

namespace DC4B.FilmsBrowser.Common.ApiVM.Membership
{
    public class LoggedInVM
    {
        public int UserId { get; set; }
        public IEnumerable<Role> Roles { get; set; }
    }
}
