﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using MongoDB.Bson;

namespace DC4B.FilmsBrowser.DAL.Mongo
{
    public class MongoRepository : IMongoRepository
    {
        private IMongoClient _client;
        private IMongoDatabase _database;

        //can use DI here
        public MongoRepository() 
        {
            _client = new MongoClient(ConfigurationManager.AppSettings.Get("MongoDb_ConnectionString"));
            _database = _client.GetDatabase(ConfigurationManager.AppSettings.Get("MongoDb_DatabaseName"));
        }

        public IMongoDatabase GetMongoDb()
        {
            return _database;
        }
    }
}
