﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.DAL.Mongo
{
    public interface IMongoRepository 
    {
        IMongoDatabase GetMongoDb();
    }
}
