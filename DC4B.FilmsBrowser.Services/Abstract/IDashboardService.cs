﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Common.ApiVM.Dashboard;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IDashboardService
    {
        DashboardVM GetData(int userId);
    }
}
