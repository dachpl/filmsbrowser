﻿using DC4B.FilmsBrowser.Entities;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IUserService : IBaseService<User>
    {
    }
}
