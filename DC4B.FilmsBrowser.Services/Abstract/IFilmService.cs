﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Common.ApiVM.Film;
using DC4B.FilmsBrowser.Entities;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IFilmService : IBaseService<Film>
    {
        FilmMetadataVM GetMetadata();
        IEnumerable<FilmListItemVM> GetFilmsList();
        IEnumerable<Actor> GetActorsByIds(int[] actorIds);
        Category GetCategoryById(int ctgId);
    }
}
