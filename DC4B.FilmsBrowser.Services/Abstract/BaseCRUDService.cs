﻿using System.Collections.Generic;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public abstract class BaseCRUDService<T> : IBaseService<T> where T : BaseEntity 
    {
        protected readonly ISqlUnitOfWork UnitOfWork;

        //not for di!
        private readonly ISqlRepository<T> _entityRepo;

        protected BaseCRUDService(ISqlUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            _entityRepo = UnitOfWork.GetRepo<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _entityRepo.Get();
        }

        public virtual int Create(T entity)
        {
            _entityRepo.Insert(entity);
            UnitOfWork.Commit();
            return entity.Id;
        }

        public virtual void Update(T entity)
        {
            _entityRepo.Update(entity);
            UnitOfWork.Commit();
        }

        public virtual void Delete(int id)
        {
            _entityRepo.Delete(id);
            UnitOfWork.Commit();
        }

        public virtual T GetById(int id)
        {
            return _entityRepo.GetById(id);
        }
    }
}
