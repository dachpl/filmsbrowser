﻿using System.Collections.Generic;
using DC4B.FilmsBrowser.Entities;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IBaseService<T> where T : BaseEntity 
    {
        #region Base CRUD operations

        IEnumerable<T> GetAll();
        int Create(T entity);
        void Update(T entity);
        void Delete(int id);
        T GetById(int id);

        #endregion
    }
}
