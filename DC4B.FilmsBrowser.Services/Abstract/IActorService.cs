﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Entities;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IActorService : IBaseService<Actor>
    {
        
    }
}
