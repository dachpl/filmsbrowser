﻿using DC4B.FilmsBrowser.Common.ApiVM.Membership;
using DC4B.FilmsBrowser.Entities;
using System.Collections;
using System.Collections.Generic;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IMembershipService
    {
        bool VerifyLogin(string login);
        bool VerifyPassword(string login, string password);
        LoggedInVM GetUserData(string userName);
    }
}
