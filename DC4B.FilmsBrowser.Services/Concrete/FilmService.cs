﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DC4B.FilmsBrowser.Common.ApiVM.Actor;
using DC4B.FilmsBrowser.Common.ApiVM.Category;
using DC4B.FilmsBrowser.Common.ApiVM.Film;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class FilmService : BaseCRUDService<Film>, IFilmService
    {
        public FilmService(ISqlUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public FilmMetadataVM GetMetadata()
        {
            var ctgs = Mapper.Map<IList<CategoryVM>>(UnitOfWork.GetRepo<Category>().Get());
            var actors = Mapper.Map<IList<ActorWithoutFilmsVM>>(UnitOfWork.GetRepo<Actor>().Get());
            return new FilmMetadataVM
            {
                Actors = actors,
                Categories = ctgs
            };
        }

        public IEnumerable<FilmListItemVM> GetFilmsList()
        {
            var filmsWithCtgs = UnitOfWork.GetRepo<Film>().Get();
            return Mapper.Map<IList<FilmListItemVM>>(filmsWithCtgs);
        }

        public IEnumerable<Actor> GetActorsByIds(int[] actorIds)
        {
            var actors = UnitOfWork.GetRepo<Actor>().Get(actor => actorIds.Contains(actor.Id));
            return actors;
        }

        public Category GetCategoryById(int ctgId)
        {
            var ctg = UnitOfWork.GetRepo<Category>().Get(category => category.Id == ctgId).Single();
            return ctg;
        }

        public override void Update(Film entity)
        {
            var filmRepo = UnitOfWork.GetRepo<Film>();
            var film = filmRepo.GetById(entity.Id);

            //why not just: film = entity; 

            film.Id = entity.Id;
            film.Category = entity.Category;
            film.Name = entity.Name;
            film.PremiereDate = entity.PremiereDate;

            film.Actors.Clear();
           
            foreach (var actor in entity.Actors)
            {
                film.Actors.Add(actor);
            }

            filmRepo.Update(film);

            UnitOfWork.Commit();

        }

        public void UpdateManyToMany(Film entity)
        {
            var filmRepo = UnitOfWork.GetRepo<Film>();
            var film = filmRepo.GetById(entity.Id);
            film.Actors.Clear();
            foreach (var actor in entity.Actors)
            {
                film.Actors.Add(actor);
            }

            film = entity;
            filmRepo.Update(film);
            UnitOfWork.Commit();
        }
    }
}
