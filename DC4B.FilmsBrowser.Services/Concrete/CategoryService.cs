﻿using System.Collections.Generic;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class CategoryService : BaseCRUDService<Category>, ICategoryService
    {
        public CategoryService(ISqlUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        //ready for custom actions
        public IEnumerable<Film> GetFilms(int categoryId)
        {
            return UnitOfWork.GetRepo<Film>().Get(film => film.CategoryId == categoryId);
        }
    }
}
