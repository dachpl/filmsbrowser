﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DC4B.FilmsBrowser.Common.ApiVM.Category;
using DC4B.FilmsBrowser.Common.ApiVM.Dashboard;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using DC4B.FilmsBrowser.DAL.SqlServer;
using System;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class InitialViewService : IInitialViewService
    {
        private readonly ISqlUnitOfWork _unitOfWork;
        public InitialViewService(ISqlUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public DashboardVM GetInitialViewData(int userId)
        {
            var user = _unitOfWork.GetRepo<User>().GetById(userId);
            return new DashboardVM
            {
                User = Mapper.Map<UserVM>(user)
            };
        }

        public void UpdateUser(UserVM userVM)
        {
            var userRepo = _unitOfWork.GetRepo<User>();
            var user = userRepo.GetById(userVM.Id);
            
            user.FirstName = userVM.FirstName;
            user.LastName = userVM.LastName;
            user.Email = userVM.Email;
            user.Address = userVM.Address;
            
            userRepo.Update(user);

            _unitOfWork.Commit();
            
        }
    }
}
