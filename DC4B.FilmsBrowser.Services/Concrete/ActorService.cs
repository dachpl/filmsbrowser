﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using System.Transactions;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class ActorService : BaseCRUDService<Actor>, IActorService
    {
        public ActorService(ISqlUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }

        public void Test()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                UnitOfWork.GetRepo<Actor>().Insert(new Actor());
                UnitOfWork.GetRepo<Category>().Insert(new Category());
                UnitOfWork.Commit();

                tran.Complete();
            }
        }
    }
}
