﻿using System;
using System.Collections.Generic;
using System.Linq;
using DC4B.FilmsBrowser.Common.ApiVM.Membership;

using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Entities.Helpers;
using DC4B.FilmsBrowser.Services.Abstract;
using Newtonsoft.Json;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class MembershipService : IMembershipService
    {
        private readonly ISqlUnitOfWork _unitOfWork;

        public MembershipService(ISqlUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool VerifyLogin(string login)
        {
            bool any = _unitOfWork.GetRepo<User>().Get(user => user.Email == login).Any();
            return any;
        }

        public bool VerifyPassword(string login, string password)
        {
            var user = _unitOfWork.GetRepo<User>().Get(u => u.Email == login).Single();
            return PasswordManager.Verify(user.Password, password);
        }

        public LoggedInVM GetUserData(string userName)
        {
            var userData = _unitOfWork.GetRepo<User>().Get(x => x.Email == userName).Single();
            var ret = new LoggedInVM
            {
                UserId = userData.Id,
                Roles = userData.Roles
            };
            return ret;
        }
    }
}
