'use strict';

/**
 * @ngdoc service
 * @name webClientApp.dashboardService
 * @description
 * # dashboardService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
    .service('DashboardService', function(BaseService) {
        angular.extend(this, BaseService);
    });
