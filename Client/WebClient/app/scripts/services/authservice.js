'use strict';

/**
 * @ngdoc service
 * @name webClientApp.loginService
 * @description
 * # loginService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
  .service('AuthService', function(appConfig, $localStorage, $http, $q, $rootScope, $httpParamSerializerJQLike, jwtHelper) {

    this.userData = {};

    this.isLoggedIn = function() {
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: appConfig.apiUrl + 'isLoggedIn',
      }).success(function(resData, status, headers, config) {
        deferred.resolve(resData);
      }).error(function(resData, status, headers, config) {
        deferred.reject(resData);
      });

      return deferred.promise;
    };

    this.login = function(email, password) {
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: appConfig.apiUrl + 'token',
        data: $httpParamSerializerJQLike({
          grant_type: 'password',
          username: email,
          password: password
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(resData, status, headers, config) {
        deferred.resolve(resData);
      }).error(function(resData, status, headers, config) {
        deferred.reject(resData);
      });

      return deferred.promise;
    };

    this.logout = function() {
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: appConfig.apiUrl + 'logout'
      }).success(function(resData, status, headers, config) {
        deferred.resolve(resData);
      }).error(function(resData, status, headers, config) {
        deferred.reject(resData);
      });

      return deferred.promise;
    };

    this.resetUserData = function() {
      $rootScope.isLoggedIn = false;
      this.userData = {};
      $localStorage.authToken = null;

    }
    this.setUserData = function(result) {

      var tokenPayload = jwtHelper.decodeToken(result.access_token);
      //console.log(tokenPayload);

      this.userData.id = tokenPayload.UserId;
      this.userData.email = tokenPayload.unique_name;
      this.userData.roles = _.isArray(tokenPayload.role) ? tokenPayload.role : [tokenPayload.role];

      $rootScope.isLoggedIn = true;
      $localStorage.authToken = result.access_token;
      //console.log(this.userData);
    };

    this.checkIfUserHasRoles = function(roles) {
      /*if user lost data after refresh for example*/
      if (!this.userData.roles){
          console.log('Searching for token in localStorage...');
          var token;
          if (!$localStorage.authToken){
              console.log('Token not found. Exit to login.');
              return false;
          }else{
            console.log('Token found. Continue.');
            token = $localStorage.authToken;
          }
          var tokenPayload = jwtHelper.decodeToken(token);
          this.userData.id = tokenPayload.UserId;
          this.userData.email = tokenPayload.unique_name;
          this.userData.roles = _.isArray(tokenPayload.role) ? tokenPayload.role : [tokenPayload.role];
          $rootScope.isLoggedIn = true;
      }else{
        console.log('Found roles in userData object');
      }
      if (_.intersection(this.userData.roles, roles).length === 0) {
        $rootScope.isLoggedIn = false;
        return false;
      }
      return true;
    };
  });