'use strict';

/**
 * @ngdoc service
 * @name webClientApp.FilmToCategory
 * @description
 * # FilmToCategory
 * Service in the webClientApp.
 */
angular.module('webClientApp')
    .service('FilmToCategoryService', function($q, $http, appConfig, BaseService) {
        angular.extend(this, BaseService);
    });
