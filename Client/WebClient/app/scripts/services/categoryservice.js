'use strict';

/**
 * @ngdoc service
 * @name webClientApp.CategoryService
 * @description
 * # CategoryService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
  .service('CategoryService', function ($http, $q, appConfig, BaseService) {
  		angular.extend(this, BaseService);
  });
