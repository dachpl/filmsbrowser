'use strict';

/**
 * @ngdoc service
 * @name webClientApp.rolesProvider
 * @description
 * # rolesProvider
 * Service in the webClientApp.
 */
angular.module('webClientApp')
  .service('RolesProvider', function () {
        this.requiredRoles = {
    		/*keys must be same as route names!*/
    		dashboard: ['Member', 'Admin'],
    		about: ['Member', 'Admin'],
    		login: ['Member', 'Admin', 'All'],
    		films: ['Member', 'Admin'],
    		actors: ['Member', 'Admin'],
    		categories: ['Member', 'Admin'],
    		me: ['Member', 'Admin'],
        };
  });
