'use strict';

/**
 * @ngdoc overview
 * @name webClientApp
 * @description
 * # webClientApp
 *
 * Main module of the application.
 */
var app = angular
  .module('webClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'angular-jwt'
  ]);
/*Extend route provider*/
app.config(function($routeProvider) {

  var originalWhen = $routeProvider.when;
  $routeProvider.when = function(path, route) {
    route.resolve || (route.resolve = {});
    angular.extend(route.resolve, {
      access: function(AuthService, $location, RolesProvider, $filter, $rootScope) {

        if (_.isNull(path) || path === '/login') {
          return;
        }
        var firstSegmentOfPath = $filter('firstPathSegment')(path);
        var requiredRoles = RolesProvider.requiredRoles[firstSegmentOfPath];

        if (_.contains(requiredRoles, 'All')) 
          return;
        
        if (!AuthService.checkIfUserHasRoles(requiredRoles)){
          $location.path("/forbidden");
        }
      }
    });
    return originalWhen.call($routeProvider, path, route);
  };

});
app.config(function($routeProvider) {
  $routeProvider
    .when('/dashboard', {
      templateUrl: 'views/dashboard.html',
      controller: 'DashboardCtrl'
    })
    .when('/about', {
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    })
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl'
    })
    .when('/films', {
      templateUrl: 'views/films.html',
      controller: 'FilmsCtrl'
    })
    .when('/films/create', {
      templateUrl: 'views/filmscreate.html',
      controller: 'FilmscreateCtrl'
    })
    .when('/films/:id/update', {
      templateUrl: 'views/filmsupdate.html',
      controller: 'FilmsupdateCtrl'
    })
    .when('/forbidden', {
      templateUrl: 'views/forbidden.html',
      controller: 'ForbiddenCtrl'
    })
    .when('/actors', {
      templateUrl: 'views/actors.html',
      controller: 'ActorsCtrl'
    })
    .when('/me', {
      templateUrl: 'views/me.html',
      controller: 'MeCtrl'
    })
    .when('/actors/create', {
      templateUrl: 'views/actorscreate.html',
      controller: 'ActorscreateCtrl'
    })
    .when('/actors/:id/update', {
      templateUrl: 'views/actorsupdate.html',
      controller: 'ActorsupdateCtrl'
    })
    .when('/categories', {
      templateUrl: 'views/category.html',
      controller: 'CategoryCtrl'
    })
    .when('/tsDemo', {
      templateUrl: 'views/tsdemo.html',
      controller: 'TsdemoCtrl'
    })
    .when('/categories/:id/films', {
      templateUrl: 'views/filmtocategory.html',
      controller: 'FilmToCategoryCtrl'
    })
    .otherwise({
      redirectTo: '/dashboard'
    });
});

app.config(function($httpProvider) {
    $httpProvider.interceptors.push('jwtInterceptor');
    $httpProvider.interceptors.push('DefaultInterceptor');
});

app.constant({
  appConfig: {
    apiUrl: 'http://localhost:51016/api/'
  },
});

app.run(function($rootScope, $location, AuthService, RolesProvider) {
   
  toastr.options = {
    positionClass: "toast-bottom-right", 
   }

  $rootScope.$on("$locationChangeStart", function(event, next, current) {

  });
});