'use strict';

/**
 * @ngdoc directive
 * @name webClientApp.directive:datepicker
 * @description
 * # datepicker
 */
angular.module('webClientApp')
	.directive('datepicker', function() {

		return {
			restrict: 'A',
			scope: {
				'defaultDate': '@'
			},
			link: function postLink(scope, element, attrs) {
				if (!_.isUndefined(scope.defaultDate)) {
					var pattern = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
					if (pattern.test(scope.defaultDate)) {
						var fullDateTime = scope.defaultDate;
						element.datepicker('setDate', new Date(fullDateTime));
						element.datepicker('update');
						element.val('');
					}else{
						console.log('datepicker directive - invalid default date format!');
					}
				}
				element.datepicker();
			}
		};
	});