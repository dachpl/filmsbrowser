'use strict';

/**
 * @ngdoc directive
 * @name webClientApp.directive:showFor
 * @description
 * # showFor
 */
angular.module('webClientApp')
	.directive('showFor', function($rootScope, AuthService, RolesProvider, $filter) {
		return {
			restrict: 'A',
			link: function postLink(scope, element, attrs) {
				element.hide();
				$rootScope.$watch('isLoggedIn', function(newVal, oldVal) {
					if (newVal) {

						var nameOfProperty = $filter('firstPathSegment')(attrs.href);
						var requiredRoles = RolesProvider.requiredRoles[nameOfProperty];

						if (_.contains(requiredRoles, 'All')) {
							element.show();
							return;
						}
						scope.show = AuthService.checkIfUserHasRoles(requiredRoles);
						if (scope.show)
							element.show();

					}
				});
			}
		};
	});