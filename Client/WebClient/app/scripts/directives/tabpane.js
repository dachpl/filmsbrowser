'use strict';

/**
 * @ngdoc directive
 * @name webClientApp.directive:tabPane
 * @description
 * # tabPane
 */
angular.module('webClientApp').directive('pane', function() {
    return {
        require: '^tabs',
        restrict: 'E',
        transclude: true,
        scope: {
            title: '@'
        },
        link: function(scope, element, attrs, tabsCtrl) {
            tabsCtrl.addPane(scope);
        },
        template: '<div class="tab-pane" role="tabpanel" ng-class="{active: selected}" ng-transclude>' +
            '</div>',
        replace: true
    };
});
