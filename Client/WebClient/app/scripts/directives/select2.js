'use strict';

/**
 * @ngdoc directive
 * @name webClientApp.directive:select2
 * @description
 * # select2
 */
angular.module('webClientApp')
	.directive('select2', function() {
		return {
			restrict: 'A',
			link: function postLink(scope, element, attrs) {
				element.select2({
					dropdownAutoWidth: true,
					containerCss : {"display":"block"}
				});
			}
		};
	});