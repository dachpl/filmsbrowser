'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('DashboardCtrl', function ($scope, DashboardService) {
  	var init = function(){
  		DashboardService.read('dashboard').then(
				function(result) {
					$scope.data = result;
				},
				function(error) {
					console.log(error);
				}
			);
  	}();
  });
