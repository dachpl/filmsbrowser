'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:ActorsCtrl
 * @description
 * # ActorsCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
	.controller('ActorsCtrl', function($scope, ActorService) {
		var init = function() {
			ActorService.read('actors').then(
				function(result) {
					$scope.actors = result;
				},
				function(error) {
					console.log('Error:', error);
				});
		}();
	});