'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:CategoryCtrl
 * @description
 * # CategoryCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('CategoryCtrl', function ($scope, CategoryService) {

  	var init = function() {
			CategoryService.read('categories').then(
				function(result) {
					$scope.categories = result;
				},
				function(error) {
					console.log('Error:', error);
				});
		}();
  });
