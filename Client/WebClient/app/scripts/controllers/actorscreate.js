'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:ActorscreateCtrl
 * @description
 * # ActorscreateCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('ActorscreateCtrl', function ($scope, ActorService, $window) {

  	$scope.create = function(actor){
  		console.log(actor);
  		ActorService.create('actors', actor).then(
				function(result) {
					console.log('Done');
					$scope.actor = {};
					$window.history.back();
				},
				function(error) {
					console.log(error);
				}
			);
  	};

  });
