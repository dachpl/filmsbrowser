'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:FilmtocategoryCtrl
 * @description
 * # FilmtocategoryCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('FilmToCategoryCtrl', function ($scope, $log, FilmToCategoryService, $routeParams, $window) {

   	var init = function(){

   		FilmToCategoryService.read('categories/'+$routeParams.id+'/films').then(
				function(result) {
					$scope.categoryFilms = result;
				},
				function(error) {
					console.log('Error:', error);
				});
   	}();
  });
