'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:FilmsupdateCtrl
 * @description
 * # FilmsupdateCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('FilmsupdateCtrl', function ($scope, FilmService, $routeParams, $window) {
  	var init = function() {
			FilmService.read('films/'+$routeParams.id+'/update').then(
				function(result) {
					$scope.film = result.Film;
					$scope.metadata = result.Metadata;
					console.log($scope.film);
				},
				function(error) {
					console.log(error);
				}
			);
		}();

		$scope.update = function(film){
			console.log(film);
			FilmService.update('films/'+$routeParams.id, film).then(
				function(result) {
					console.log('Done');
					$window.history.back();
				},
				function(error) {
					console.log(error);
				}
			);
		}
  });
