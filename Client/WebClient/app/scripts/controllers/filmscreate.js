'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:FilmscreateCtrl
 * @description
 * # FilmscreateCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
	.controller('FilmscreateCtrl', function($scope, FilmService, $window) {
		var init = function() {
			FilmService.read('films/create').then(
				function(result) {
					$scope.metadata = result;
				},
				function(error) {
					console.log(error);
				}
			);
		}();

		$scope.create = function(film){
			console.log(film);
			FilmService.create('films', film).then(
				function(result) {
					console.log('Done');
					$scope.film = {};
					$window.history.back();
				},
				function(error) {
					console.log(error);
				}
			);
		}
	});