'use strict';

describe('Directive: showFor', function () {

  // load the directive's module
  beforeEach(module('webClientApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<show-for></show-for>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the showFor directive');
  }));
});
