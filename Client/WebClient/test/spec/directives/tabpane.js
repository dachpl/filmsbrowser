'use strict';

describe('Directive: pane', function () {

  // load the directive's module
  beforeEach(module('webClientApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<pane></pane>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the tabPane directive');
  }));
});
