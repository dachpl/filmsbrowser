'use strict';

describe('Filter: firstPathSegment', function () {

  // load the filter's module
  beforeEach(module('webClientApp'));

  // initialize a new instance of the filter before each test
  var firstPathSegment;
  beforeEach(inject(function ($filter) {
    firstPathSegment = $filter('firstPathSegment');
  }));

  it('should return the input prefixed with "firstPathSegment filter:"', function () {
    var text = 'angularjs';
    expect(firstPathSegment(text)).toBe('firstPathSegment filter: ' + text);
  });

});
