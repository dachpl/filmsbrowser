'use strict';

describe('Service: FilmToCategory', function () {

  // load the service's module
  beforeEach(module('webClientApp'));

  // instantiate service
  var FilmToCategory;
  beforeEach(inject(function (_FilmToCategory_) {
    FilmToCategory = _FilmToCategory_;
  }));

  it('should do something', function () {
    expect(!!FilmToCategory).toBe(true);
  });

});
