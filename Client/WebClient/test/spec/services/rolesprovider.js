'use strict';

describe('Service: rolesProvider', function () {

  // load the service's module
  beforeEach(module('webClientApp'));

  // instantiate service
  var rolesProvider;
  beforeEach(inject(function (_rolesProvider_) {
    rolesProvider = _rolesProvider_;
  }));

  it('should do something', function () {
    expect(!!rolesProvider).toBe(true);
  });

});
