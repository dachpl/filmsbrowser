'use strict';

describe('Service: defaultInterceptor', function () {

  // load the service's module
  beforeEach(module('webClientApp'));

  // instantiate service
  var defaultInterceptor;
  beforeEach(inject(function (_defaultInterceptor_) {
    defaultInterceptor = _defaultInterceptor_;
  }));

  it('should do something', function () {
    expect(!!defaultInterceptor).toBe(true);
  });

});
