'use strict';

describe('Controller: FilmtocategoryCtrl', function () {

  // load the controller's module
  beforeEach(module('webClientApp'));

  var FilmtocategoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FilmtocategoryCtrl = $controller('FilmtocategoryCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
