'use strict';

describe('Controller: ActorscreateCtrl', function () {

  // load the controller's module
  beforeEach(module('webClientApp'));

  var ActorscreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActorscreateCtrl = $controller('ActorscreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
