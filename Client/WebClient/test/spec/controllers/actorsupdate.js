'use strict';

describe('Controller: ActorsupdateCtrl', function () {

  // load the controller's module
  beforeEach(module('webClientApp'));

  var ActorsupdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActorsupdateCtrl = $controller('ActorsupdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
