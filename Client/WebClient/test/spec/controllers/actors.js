'use strict';

describe('Controller: ActorsCtrl', function () {

  // load the controller's module
  beforeEach(module('webClientApp'));

  var ActorsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActorsCtrl = $controller('ActorsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
